//? Si mon utilisateur n'est pas admin je redirige vers l'accueil
if (user.admin == 0) window.location.replace("../../article/article.html");

/**
 * @desc Fait appel au php pour supprimer un article
 * @param string id - Contient l'id de l'article
 * @return void - Ne retourne rien
 */
function deleteArticle(id) {
    $.ajax({
        url: "../../php/admin/article.php", // URL cible
        type: "POST", // Type de méthode de requête HTTP
        dataType: "json", // Type de réponse attendue
        data: { // Donnée(s) à envoyer s'il y en a
            choice: "delete",
            id
        },
        success: () => {
            $("#tr_" + id).remove(); // Je retire la ligne du tableau associé à l'article
        }
    });
}

$.ajax({
    url: "../../php/admin/article.php", // URL cible
    type: "GET", // Type de méthode de requête HTTP
    dataType: "json", // Type de réponse attendue
    data: { // Donnée(s) à envoyer s'il y en a
        choice: "select"
    },
    success: (res) => {
        if (res.success) {
            res.articles.forEach(article => {
                const tr = $("<tr></tr>"); // Je crée une nouvelle ligne
                tr.attr("id", "tr_" + article.id) // J'ajoute un id à mon tr

                const title = $("<td></td>").text(article.name); // Je crée une case pour le nom
                const desc = $("<td></td>"); // Je crée une case pour la description
                let desc_txt = article.description.substring(0,20);
                if (article.description.length > 20) desc_txt += "...";
                desc.text(desc_txt);
                

                const imagectn = $("<td></td>"); // Je crée une case pour l'image
                let img = "Aucune image"; // Par défaut il n'y a aucune image
                //? S'il y a une image alors je crée un élement img et je lui affecte la source
                if (article.image) img = $("<img>").attr("src", "../../assets/" + article.image);
                imagectn.append(img); // J'ajoute mon image à mon td

                // Je crée une case pour mon auteur et j'y affecte le texte associé
                const author = $("<td></td>").text(article.author);

                // Je crée une case pour accueillir les catégories et par faut j'y place le texte "Aucune catégorie"
                const cats_ctn = $("<td></td>").text("Aucune catégorie");
                //? Si j'ai des catégories associées à mon article alors je remplace le texte par le nom de ces catégories
                if (article.categories.length) cats_ctn.text(article.categories.join(', '));

                const updatectn = $("<td></td>"); // Je crée une case pour contenir mon bouton
                const updatebtn = $("<button></button>"); // Je crée le bouton pour la mise à jour
                updatebtn.addClass("btn ocean action_btn"); // J'ajoute des classes sur le bouton pour le style
                updatebtn.html("<i class='fa fa-pencil' aria-hidden='true'></i>"); // J'ajoute un texte au lien
                updatectn.append(updatebtn); // J'ajoute le boutton au td

                updatebtn.click(() => {
                    window.location.replace("manage_article/manage_article.html?id=" + article.id); // Je redirige vers la page du formulaire avec paramètre id de mon article sur lequel j'itère en paramètre
                });

                const delctn = $("<td></td>"); // Je crée une case pour contenir mon bouton
                const delbtn = $("<button></button>"); // Je crée le bouton pour la suppression
                delbtn.html("<i class='fa fa-trash' aria-hidden='true'></i>"); // J'ajoute le contenu du bouton, ici une icone de poubelle
                delbtn.addClass("btn salmon action_btn"); // J'ajoute des classes sur le bouton pour le style
                delctn.append(delbtn);

                // J'ajoute un écouteur d'événement clic sur le bouton
                delbtn.click(() => {
                    if (confirm("Voulez-vous vraiment supprimer l'article ?")) {
                        // J'appelle la fonction wantToDelete pour demander la suppression de l'article
                        deleteArticle(article.id);
                    }
                });

                tr.append(title, desc, imagectn, author, cats_ctn, updatectn, delctn); // J'ajoute toutes mes cases dans ma ligne
                $("tbody").append(tr); // J'ajoute ma ligne à ma table
            });
            
            $("td").addClass("text-left"); // J'ajoute une classe à tous les td
        } else alert(res.error);
    }
});

// Au clic de la div "Ajouter un article"
$("header div.btn").click(() => {
    // Je redirige vers la page du formulaire
    window.location.replace("manage_article/manage_article.html");
});